# README #



Omowumi Lynda Ademola
CS441 Mobile Game Design
February 8, 2017
Assignment 02

Create a game like 2048

Resources:
cs441-2048 - Patrick Madden

Notes:
* Looks best on iPhone 7 simulator
* Swipe within the game box
----
commit d95e29086ceec9597fc8fb9ef62758b0d42268f9
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Feb 13 22:17:56 2017 -0500

    added message for winning game

commit 8c5ba2a2198404c07447e72efa0d76354371df5a
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Feb 13 22:09:04 2017 -0500

    Made restart button disappear after being clicked

commit 8abe4cf562cf8f9bec5ce063f065768192a470ba
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Feb 9 19:35:14 2017 -0500

    moved restart button up

commit c655f11eccc45c3d7896831042c71195fa3f6b56
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Feb 9 02:36:35 2017 -0500

    moved readme location

commit 110bc452a4c257794837ade68492fe6d3d3193bc
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Feb 9 02:35:31 2017 -0500

    added readme and cleaned up files a bit

commit c29539dcc375ba23a6c803819ed8c2d579b67a20
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Feb 9 02:21:19 2017 -0500

scraped alert box, just use button

commit 246d65d4d8cd014e79df526c528db77c000fb202
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Feb 9 01:48:37 2017 -0500

game works still working on alert boxes

commit 2199e8d8f5e99d5d1f2f0f778b580c966ef565ad
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Feb 9 00:36:38 2017 -0500

scraped my game logic used prof's game logic

commit 3c98690f03cbdd79427255ff64c53ad35500d119
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Wed Feb 8 22:13:17 2017 -0500

programatically add new tile

commit 8de55b1695ea7718fe78a6173bc6375bb75ffe7a
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Wed Feb 8 21:07:37 2017 -0500

made board prettier and randomized tiles

commit 9f150e40c963f41297aaedc4e29aa54a236e7105
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Feb 7 23:21:19 2017 -0500

got a tile updating and moving on comparison

commit 9bd4af972c334be93f7ac9c6f142de0919cfafb5
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Feb 7 23:03:50 2017 -0500

working on checking if adjacent tiles match

commit 3560d45afc5c40288736cc6c820aa2406273d333
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Feb 7 21:55:27 2017 -0500

getting swiping tiles up and running

commit 68f5f500c80e06f414f446cd3909191bc3b45edc
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Feb 7 19:27:16 2017 -0500

got tile programatically generated

commit ec9f97e39aa89981bcc565b621a57c37040a4d0c
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Feb 7 19:16:59 2017 -0500

retrying the array of tiles

commit 035b292dd4f4a9d9936d9b747830feaf812fd2fa
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Feb 7 17:32:12 2017 -0500

getting tiles to be created programmatically

commit 4f2ff2123924340329fe921140f43749f459ee35
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Feb 6 21:23:21 2017 -0500

initial commit

----


