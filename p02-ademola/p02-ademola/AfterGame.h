//
//  AfterGame.h
//  p02-ademola
//
//  Created by Lynda on 2/13/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AfterGame : UIView
@property (nonatomic, strong) IBOutlet UIButton *restart;
@property (nonatomic, strong) UILabel *afterGameMessage;
@end
