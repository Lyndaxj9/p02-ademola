//
//  GameLogic.h
//  p02-ademola
//
//  Created by Lynda on 2/7/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameLogic : NSObject {
    int matrix[4][4];
}

-(void)initMatrix;
-(void)logMatrix;

@end
