//
//  GameView.h
//  p02-ademola
//
//  Created by Lynda on 2/7/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TileObject.h"
#import "Restart.h"
#import "ViewController.h"

@interface GameView : UIView {
    int xPos, yPos;
    float xMax, yMax, xMin, yMin;
}
@property (nonatomic, strong) NSArray *directions;
//@property (nonatomic, strong) NSMutableArray *merged;
@property (nonatomic, strong) NSMutableArray *removed;
@property (nonatomic, strong) NSMutableArray *tileUpdates;
@property (nonatomic, strong) NSMutableArray *tileObjects;

@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGReg;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGReg;
@property (nonatomic, strong) UISwipeGestureRecognizer *downSwipeGReg;
@property (nonatomic, strong) UISwipeGestureRecognizer *upSwipeGReg;

@property (nonatomic, strong) UILabel *goLabel;

@property (nonatomic) bool win;
@property (nonatomic, strong) UIAlertController *gameOver;
@property (nonatomic, strong) UIAlertController *gameWin;
@property (nonatomic, strong) UIAlertAction *ok;
@property (nonatomic, strong) UIAlertAction *gameNew;

-(void)handleSwipes:(UISwipeGestureRecognizer *)sender;
//-(void)gameOverAlert;
-(IBAction)reStartGame:(UIButton *)button;

@end
