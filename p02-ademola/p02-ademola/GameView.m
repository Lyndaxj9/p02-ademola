//
//  GameView.m
//  p02-ademola
//
//  Created by Lynda on 2/7/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize tileObjects, directions, tileUpdates, removed;
@synthesize goLabel;
@synthesize win;
@synthesize gameOver, gameWin, ok, gameNew;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self.leftSwipeGReg = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.rightSwipeGReg = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.downSwipeGReg = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    self.upSwipeGReg = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    
    self.leftSwipeGReg.direction = UISwipeGestureRecognizerDirectionLeft;
    self.rightSwipeGReg.direction = UISwipeGestureRecognizerDirectionRight;
    self.downSwipeGReg.direction = UISwipeGestureRecognizerDirectionDown;
    self.upSwipeGReg.direction = UISwipeGestureRecognizerDirectionUp;
    
    [self addGestureRecognizer:self.leftSwipeGReg];
    [self addGestureRecognizer:self.rightSwipeGReg];
    [self addGestureRecognizer:self.downSwipeGReg];
    [self addGestureRecognizer:self.upSwipeGReg];
    
    win = false;
    
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        tileObjects = [[NSMutableArray alloc] init];
        xPos = 12;
        yPos = 12;
        for(int i = 0; i<4; ++i) {
            for(int j = 0; j<4; ++j) {
                TileObject *tile = [[TileObject alloc] initWithFrame:CGRectMake(xPos+(j*12+j*60), yPos, 60, 60)];
                [tileObjects addObject:tile];
                [tile setTileValue:0];
                //[self addSubview:tile];
            }
            yPos = yPos + 12 + 60;
        }
        
        //from stackoverflow 9678373
        int upBound = 16;
        int lowBound = 0;
        int rndValue01 = lowBound + arc4random() % (upBound - lowBound);
        int rndValue02 = lowBound + arc4random() % (upBound - lowBound);
        while(rndValue02 == rndValue01) {
            NSLog(@"random start pos: %d, %d", rndValue01, rndValue02);
            rndValue02 = lowBound + arc4random() % (upBound - lowBound);
        }
        NSLog(@"random start pos: %d, %d", rndValue01, rndValue02);
        
        TileObject *tile01 = tileObjects[rndValue01];
        TileObject *tile02 = tileObjects[rndValue02];
        //TileObject *tile01 = tileObjects[4];
        //TileObject *tile02 = tileObjects[8];
        [tile01 setTileValue:2];
        [tile02 setTileValue:2];
        [tile01 changeText:@"2"];
        [tile02 changeText:@"2"];
        [self addSubview:tile01];
        [self addSubview:tile02];
        
        NSArray *north = @[@[@( 0), @( 4), @( 8), @(12)],
                           @[@( 1), @( 5), @( 9), @(13)],
                           @[@( 2), @( 6), @(10), @(14)],
                           @[@( 3), @( 7), @(11), @(15)]];
        NSArray *south = @[@[@(12), @( 8), @( 4), @( 0)],
                           @[@(13), @( 9), @( 5), @( 1)],
                           @[@(14), @(10), @( 6), @( 2)],
                           @[@(15), @(11), @( 7), @( 3)]];
        NSArray *east  = @[@[@( 3), @( 2), @( 1), @( 0)],
                           @[@( 7), @( 6), @( 5), @( 4)],
                           @[@(11), @(10), @( 9), @( 8)],
                           @[@(15), @(14), @(13), @(12)]];
        NSArray *west  = @[@[@( 0), @( 1), @( 2), @( 3)],
                           @[@( 4), @( 5), @( 6), @( 7)],
                           @[@( 8), @( 9), @(10), @(11)],
                           @[@(12), @(13), @(14), @(15)]];
        
        directions = @[north, south, east, west];
    }
    
    return self;
}

-(void)handleSwipes:(UISwipeGestureRecognizer *)sender
{
    //NORTH
    if(sender.direction == UISwipeGestureRecognizerDirectionUp)
    {
        //send south array
        [self checkTiles:directions[0]];
        
        for(int i = 0; i < 16; ++i)
        {
            TileObject *aTile = tileObjects[i];
            if([aTile tileValue] == 0) {
                if([aTile isDescendantOfView:self]){
                    [aTile removeFromSuperview];
                }
            } else {
                if(![aTile isDescendantOfView:self]){
                    [self addSubview:aTile];
                }
            }
        }
    }
    
    //SOUTH
    if(sender.direction == UISwipeGestureRecognizerDirectionDown)
    {
        //send south array
        [self checkTiles:directions[1]];
        
        for(int i = 0; i < 16; ++i)
        {
            TileObject *aTile = tileObjects[i];
            if([aTile tileValue] == 0) {
                if([aTile isDescendantOfView:self]){
                    [aTile removeFromSuperview];
                }
            } else {
                if(![aTile isDescendantOfView:self]){
                    [self addSubview:aTile];
                }
            }
        }
    }
    
    //EAST
    if(sender.direction == UISwipeGestureRecognizerDirectionRight)
    {
        //send south array
        [self checkTiles:directions[2]];
        
        for(int i = 0; i < 16; ++i)
        {
            TileObject *aTile = tileObjects[i];
            if([aTile tileValue] == 0) {
                if([aTile isDescendantOfView:self]){
                    [aTile removeFromSuperview];
                }
            } else {
                if(![aTile isDescendantOfView:self]){
                    [self addSubview:aTile];
                }
            }
        }
    }
    
    //WEST
    if(sender.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        //send south array
        [self checkTiles:directions[3]];
        
        for(int i = 0; i < 16; ++i)
        {
            TileObject *aTile = tileObjects[i];
            if([aTile tileValue] == 0) {
                if([aTile isDescendantOfView:self]){
                    [aTile removeFromSuperview];
                }
            } else {
                if(![aTile isDescendantOfView:self]){
                    [self addSubview:aTile];
                }
            }
        }
    }
    
    if(win) {
        NSLog(@"YOU WON");
        [self gameOverView:@"YOU WON"];
    }
}

-(void)checkTiles:(NSArray *)tileDirection
{
    int newValues[4];
    int merged[4];
    
    for(int row = 0; row < 4; ++row) {
        NSArray *thisRow = [tileDirection objectAtIndex:row];
        
        for(int i = 0; i<4; ++i) {
            newValues[i] = 0;
            merged[i] = 0;
        }
        
        int index = 0;
        
        for(int j = 0; j<4; ++j) {
            NSNumber *tileIndex = [thisRow objectAtIndex:j];
            TileObject *tile = [tileObjects objectAtIndex:[tileIndex integerValue]];
            
            if([tile tileValue] > 0) {
                if(index == 0) {
                    newValues[index++] = [tile tileValue];
                } else {
                    if ((newValues[index - 1] == [tile tileValue]) && (!merged[index - 1])) {
                        newValues[index - 1] = newValues[index - 1] * 2;
                        if(newValues[index-1] == 2048) {
                            win = true;
                        }
                        merged[index - 1] = 1;
                    } else {
                        newValues[index++] = [tile tileValue];
                    }
                }
            }
        }
        
        for (int k = 0; k < 4; ++k)
        {
            NSNumber *tileIndex = [thisRow objectAtIndex:k];
            TileObject *tile = [tileObjects objectAtIndex:[tileIndex integerValue]];
            [tile setTileValue:newValues[k]];
            NSString *newLabel = [NSString stringWithFormat:@"%d", [tile tileValue]];
            [tile changeText:newLabel];
        }
    }
    
    int randomTile = [self randTile];
    if(randomTile == -1) {
        NSLog(@"GAME OVER");
        //ViewController *vc = [ViewController alloc];
        [self gameOverView:@"GAME OVER"];
    } else {
        TileObject *tile = [tileObjects objectAtIndex:randomTile];
        int randNum = arc4random();
        if(randNum < (pow(2, 32))/60000) {
            [tile setTileValue:4];
        } else {
            [tile setTileValue:2];
        }
        NSString *newLabel = [NSString stringWithFormat:@"%d", [tile tileValue]];
        [tile changeText:newLabel];
    }
    
}

-(int)randTile {
    int tilePos = -1;
    int i = -1;
    
    int upBound = 16;
    int lowBound = 0;
    int rndValue = lowBound + arc4random() % (upBound - lowBound);
    
    TileObject *tile = tileObjects[rndValue];
    tilePos = rndValue;
    if([tile tileValue] != 0) {
        while (i < 15 && [tile tileValue] != 0){
            i++;
            tile = tileObjects[i];
        }
        if([tile tileValue] == 0) {
            tilePos = i;
        } else {
            tilePos = -1;
        }
    }
    
    
    return tilePos;
}

/*
-(void)gameOverAlert {
    gameOver = [UIAlertController
                                 alertControllerWithTitle:@"GAME OVER!"
                                 message:@"You couldn't get 2048"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    ok = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    
    gameNew = [UIAlertAction
          actionWithTitle:@"NEW GAME"
          style:UIAlertActionStyleDefault
          handler:^(UIAlertAction * action) {
              [self reStartGame];
          }];
    
    [gameOver addAction:gameNew];
    ViewController *vc = [[ViewController alloc]init];
    [vc presentViewController:gameOver animated:YES completion:nil];
}
*/

-(void)gameOverView:(NSString *)message {
    goLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 400, 200, 40)];
    [goLabel setBackgroundColor:[UIColor clearColor]];
    [goLabel setText:message];
    //[goLabel setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:goLabel];
    goLabel.userInteractionEnabled = NO;
    
    UIButton *restartGame = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    restartGame.frame = CGRectMake(100, 200, 100, 18);
    [restartGame setBackgroundColor:[UIColor whiteColor]];
    [restartGame setTitle:@"Restart" forState:UIControlStateNormal];
    [restartGame addTarget:self action:@selector(reStartGame:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:restartGame];
    restartGame.userInteractionEnabled = YES;

}


-(IBAction)reStartGame:(UIButton *)button {
    NSLog(@"reStartGame called.");
    for(int i = 0; i < 16; ++i) {
        TileObject *aTile = [tileObjects objectAtIndex:i];
        [aTile setTileValue:0];
        if([aTile isDescendantOfView:self]){
            [aTile removeFromSuperview];
        }
    }
    
    
    int upBound = 16;
    int lowBound = 0;
    int rndValue01 = lowBound + arc4random() % (upBound - lowBound);
    int rndValue02 = lowBound + arc4random() % (upBound - lowBound);
    while(rndValue02 == rndValue01) {
        NSLog(@"random start pos: %d, %d", rndValue01, rndValue02);
        rndValue02 = lowBound + arc4random() % (upBound - lowBound);
    }
    
    TileObject *tile01 = tileObjects[rndValue01];
    TileObject *tile02 = tileObjects[rndValue02];
    //TileObject *tile01 = tileObjects[4];
    //TileObject *tile02 = tileObjects[8];
    [tile01 setTileValue:2];
    [tile02 setTileValue:2];
    [tile01 changeText:@"2"];
    [tile02 changeText:@"2"];
    [self addSubview:tile01];
    [self addSubview:tile02];
    [button removeFromSuperview];
    [goLabel removeFromSuperview];
    
}


@end
