//
//  Restart.m
//  p02-ademola
//
//  Created by Lynda on 2/13/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "Restart.h"

@implementation Restart

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id)initWithFrame:(CGRect)frame
{
    NSLog(@"initWithFrame Button");
    self = [super initWithFrame:CGRectMake(100, 400, 100, 18)];
    if(self)
    {
        //self.frame = CGRectMake(100, 200, 100, 18);
        [self setTitle:@"Restart" forState:UIControlStateNormal];
        self.userInteractionEnabled = NO;

    }
    
    return self;
}

-(void)revealButton
{
    NSLog(@"buttonRevealed");
    //self.hidden = NO;
    [self setHidden:YES];
    self.userInteractionEnabled = YES;
}
@end
