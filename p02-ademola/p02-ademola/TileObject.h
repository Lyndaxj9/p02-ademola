//
//  TileObject.h
//  p02-ademola
//
//  Created by Lynda on 2/6/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TileObject : UIView
@property (nonatomic, strong) IBOutlet UIView *contents;
@property (nonatomic, strong) IBOutlet UILabel *tileNum;
@property (nonatomic) int tileValue;

-(void)changeText:(NSString*)newString;
@end
