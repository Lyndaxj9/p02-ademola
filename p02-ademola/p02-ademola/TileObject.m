//
//  TileObject.m
//  p02-ademola
//
//  Created by Lynda on 2/6/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "TileObject.h"

@implementation TileObject
@synthesize contents, tileNum, tileValue;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame
{
    NSLog(@"initWithFrame");
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    
    return self;
}

-(void)setup
{
    [[NSBundle mainBundle] loadNibNamed:@"tile" owner:self options:nil];
    
    [self addSubview:self.contents];
    [tileNum setText:@"-"];
    
    self.contents.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Our WidgetView is a UIView that contains a UIView, we need to establish
    //  the relation with autolayout. We'll want the self.view to be 200x100 pixels
    //  and we'll have the superview (WidgetView) stick to the edges (i.e. same size).
    
    // width and edges   H:|[self.view(200)]|
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contents(60)]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(contents, self)]];
    
    // height and edges   V:|[self.view(100)]|
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[contents(60)]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(contents, self)]];
    
}

-(void)changeText:(NSString*)newString
{
    [tileNum setText:newString];
}

@end
